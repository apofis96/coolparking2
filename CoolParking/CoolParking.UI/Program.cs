﻿using System;
using CoolParking.BL.Services;
using CoolParking.BL.Models;
using System.IO;
using System.Reflection;

namespace CoolParking.UI
{
    class Program
    {


        public Program()
        {
            /*withdrawTimer = new FakeTimerService();
            logTimer = new FakeTimerService();
            logService = A.Fake<ILogService>();
            parkingService = new ParkingService(_withdrawTimer, _logTimer, _logService);*/
        }
        static void Main(string[] args)
        {
            TimerService withdrawTimer = new TimerService(Settings.chargeoffPeriod);
            TimerService logTimer = new TimerService(Settings.loggingPeriod);
            LogService logService = new LogService($@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log");
            ParkingService parkingService = new ParkingService(withdrawTimer, logTimer, logService);
            //Console.WriteLine(parkingService.GetCapacity());
            string menuItems = @"1 : Display Parking balance.
2 : Display amount of current earned money.
3 : Display available/occupied parking places.
4 : Display Transactions for current period.
5 : Display the transaction history.
6 : Display Vehicles in the parking lot.
7 : Put Vehicle in the parking lot.
8 : Pick up Vehicle from the parking lot.
9 : Top up the balance of Vehicle.
0 : Exit.
h : Display this menu.";
            Console.WriteLine(menuItems);
            bool loop = true;
            while (loop)
            {
                Console.Write("Main menu input: ");
                char menu = Console.ReadKey().KeyChar;
                Console.WriteLine("\n================================");
                switch (menu)
                {
                    case '1':
                        Console.WriteLine($"Parking balance: {parkingService.GetBalance()}.");
                        break;
                    case '2':
                        Console.WriteLine($"Current earned money: {parkingService.CurrentSum}.");
                        break;
                    case '3':
                        Console.WriteLine("Parking places: {0} available, {1} occupied.",parkingService.GetFreePlaces(), parkingService.GetOccupiedPlaces());
                        break;
                    case '4':
                        Console.WriteLine("Transactions:");
                        foreach (TransactionInfo tr in parkingService.GetLastParkingTransactions())
                        {
                            Console.WriteLine(tr.ToString());
                        }
                        break;
                    case '5':
                        try
                        {
                            Console.WriteLine("Transaction history:\n" + parkingService.ReadFromLog());
                        }
                        catch (InvalidOperationException e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        break;
                    case '6':
                        Console.WriteLine("Vehicles:");
                        foreach(Vehicle vh in parkingService.GetVehicles())
                        {
                            Console.WriteLine(vh.ToString());
                        }
                        break;
                    case '7':
                        PutVehicle(parkingService);
                        break;
                    case '8':
                        PickVehicle(parkingService);
                        break;
                    case '9':
                        TopVehicle(parkingService);
                        break;
                    case '0':
                        loop = false;
                        break;
                    case 'h':
                    default:
                        Console.WriteLine(menuItems);
                        break;
                }
            }
        }
        static void PutVehicle(ParkingService parkingService)
        {
            string plate;
            int type;
            decimal balance;
            Console.Write("Adding Vehicle:\nLicense plate:\n0 : Random\n1 : Manual\ne : Exit\nInput:");
            char menu = Console.ReadKey().KeyChar;
            Console.WriteLine();
            switch (menu)
            {
                case '1':
                    do
                    {
                        Console.Write("Input license plate (format AA-0000-AA): ");
                        plate = Console.ReadLine();

                        if (!Vehicle.ValidateRegistrationPlateNumber(plate))
                        {
                            Console.WriteLine("Wrong plate format");
                        }
                        else
                            break;
                    } while (true);
                    break;
                case 'e':
                    return;
                case '0':
                default:
                    plate = Vehicle.GenerateRandomRegistrationPlateNumber();
                    break;
            }
            Console.WriteLine("License plate: " + plate);
            Console.WriteLine("Chose Vehicle type:");
            int types = -1;
            foreach (var item in Enum.GetValues(typeof(VehicleType)))
            {
                types++;
                Console.WriteLine($"{(int)item} : {item}.");
            }
            do
            {
                Console.Write("Input: ");
                string input = Console.ReadLine();
                type = int.TryParse(input, out type) ? type : -1;
                if (type == -1 || type < 0 || type > types)
                {
                    Console.WriteLine("Wrong input");
                }
                else
                    break;
            }
            while (true);
            do
            {
                Console.WriteLine("Vehicle balance: ");
                balance = decimal.TryParse(Console.ReadLine(), out balance) ? balance : -1;
                if (balance < 0)
                {
                    Console.WriteLine("Wrong input, balance must be positive number");
                }
                else
                    break;
            }
            while (true);
            try
            {
                parkingService.AddVehicle(new Vehicle(plate, (VehicleType)type, balance));
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }
            catch(InvalidOperationException e)
            {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine("Done");

        }
        static void PickVehicle(ParkingService parkingService)
        {
            
            Console.WriteLine("Pick up Vehicle");
            try 
            {
                parkingService.RemoveVehicle(SelectVehicle(parkingService));
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine("Done");
        }
        static void TopVehicle(ParkingService parkingService)
        {
            Console.WriteLine("Top up Vehicle");
            try
            {
                string vh = SelectVehicle(parkingService);
                decimal input;
                do
                {
                    Console.Write("Input emount: ");
                    input = decimal.TryParse(Console.ReadLine(), out input) ? input : -1;
                    if (input < 0)
                    {
                        Console.WriteLine("Wrong input");
                    }
                    else
                        break;
                }
                while (true);

                parkingService.TopUpVehicle(vh, input);
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine("Done");
        }
        static string SelectVehicle(ParkingService parkingService)
        {
            int i = -1, input;
            foreach (Vehicle vh in parkingService.GetVehicles())
            {
                Console.WriteLine($"{++i} : {vh.Id}");
            }
            do
            {
                Console.Write("Input: ");
                input = int.TryParse(Console.ReadLine(), out input) ? input : -1;
                if (input < 0 || input > i)
                {
                    Console.WriteLine("Wrong input");
                }
                else
                    return parkingService.GetVehicles()[input].Id;
            }
            while (true);
        }

    }
}
